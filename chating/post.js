var messages = [];
var replyMessage;
var isEditing = false;
var mid = 0;
var ti = 0;
/**
  * 实时刷新
  */
function showmessage() {
  var ajax = new XMLHttpRequest();
  // 从服务器获取并处理数据
  ajax.onreadystatechange = function(){
      if(ajax.readyState==4) {
          eval('var data = '+ajax.responseText);
          var s = "";
          for(var i = 0 ; i < Object.keys(data).length;i++){
            var message = data[i];
            if(message['replyurl'] !== "") {
              s += "<div id=\"obj_" + ti + "\" class=\"t main-bg\">"
              s += "<dl class=\"message cc\"><dt>";
              s += "<div class=\"cc\">"+ message["from"] + message["vip"] +"</div>";
              s += "<a href=\"u.php?action=show&uid="+ message["fromuid"] + " target=\"_blank\">" + message["face"] + "</a></dt>";
              s += "<dd><div class=\"o_bubble_a\"><b class=\"o_bubble_a_top\"></b><div class=\"o_bubble_a_bg cc\">" + message["content"] + "<div class=\"btns\">";
              s += "<input type=\"button\" value=\"回复\" class=\"record_btn\" onclick=\"getReplyMessage(" + ti + ");\" />";
              s += "<input type=\"button\" value=\"忽略\" class=\"ignore_btn\" onclick=\"ignoreMessage(" + ti + ");\" /></div>"
              s += "</div></div></dd></dl></div>";
            }
            messages.push(message);
            mid = data[0]['mid'];
            ti += 1;
          }
          // 开始向页面时追加信息
          var showmessage = document.getElementById("messages");
          showmessage.innerHTML = s + showmessage.innerHTML;
          showmessage.scrollTop = showmessage.scrollHeight-showmessage.style.height;
      }
  }
  ajax.open('get','./newmessage.php?mid=' + mid);
  ajax.overrideMimeType("text/html;charset=gb2312");//设定以gb2312编码识别数据
  ajax.send(null);  
}

/**
  * 获取回复内容
  */
function getReplyMessage(index) {
  isEditing = true;
  var ajax = new XMLHttpRequest();
  // 从服务器获取并处理数据
  ajax.onreadystatechange = function(){
      if(ajax.readyState==4) {
        var response = ajax.responseText;
        var split_index = response.indexOf('}c:{');
        var title = getObj('atc_title');
        title.value = response.substr(3, split_index - 3);
        var textarea = getObj('textarea'); 
        textarea.value = response.substr(split_index + 4, response.length - split_index - 4);
        getObj('replyform').action ="readmsgandredirect.php?mid=" +  messages[index]['mid'] + "&action=chat&" + messages[index]['replyurl'];
        getObj('replyObj').name = ('obj_' + index);
        var current = getObj('obj_' + index);
        current.appendChild(getObj('replyObj'));
        textarea.scrollTop = textarea.scrollHeight;
      }
  }
  var url = './ajax.php?action=chat&'+ messages[index]['replyurl'];
  ajax.open('get', url);
  ajax.overrideMimeType("text/html;charset=gb2312");//设定以gb2312编码识别数据
  ajax.setRequestHeader("Content-Type","text/html;charset=GBK");
  ajax.send(null);  
}

/**
 * 忽略该消息并设置为已读
 */
function ignoreMessage(index) {
  var ajax = new XMLHttpRequest();
  // 返回数据并成功
  ajax.onreadystatechange = function(){
    if(ajax.readyState==4) {
      var replyObj = getObj('replyObj');
      if(replyObj.name == 'obj_' + index) {
        getObj('hideObj').appendChild(replyObj);
      }
      getObj('messages').removeChild(getObj('obj_' + index));
    }
  }
  ajax.open('get', './readmsgandredirect.php?mid=' + messages[index]['mid']);
  ajax.send(null);  
}

/**
  * 发表回帖
  */
function replyPost(o) {
  ajax.submit(o, function() {
    isEditing = false;
    var replyObj = getObj('replyObj');
    getObj('hideObj').appendChild(replyObj);
    getObj('messages').removeChild(getObj(replyObj.name));
  });
}

/**
 * 快捷设置
 */
function quickpost(event){
  if((event.ctrlKey && event.keyCode == 13) || (event.altKey && event.keyCode == 83)){
    getObj('write_button').click();
  }
}

function reload() {
  if(!isEditing) {
    showmessage();
  }
}
showmessage();