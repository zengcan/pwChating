var charset='$db_charset';
var bbsurl ="{$B_url}";
var editor = null;

function WysiwygConfig() {
  this.baseURL = document.baseURI || document.URL;
  if (this.baseURL && this.baseURL.match(/(.*)\/([^\/]?)/)) {
    this.baseURL = RegExp.$1 + "/";
  }
  this.imgURL = imgpath + "/post/c_editor/";

  this.btnList = {
    bold: [ true, editorcode, true ],
    forecolor: [ true, showcolor, false ],
    hilitecolor: [ true, showcolor, false ],
    createlink: [ true, showcreatelink, false ],
    unlink: [ true, editorcode, false ],
    insertimage: [ true, insertImage, false ]
  };
  this.selList = [];
};

function loadEditor(e) {
  var e = is_ie ? event : e;
  var o = e.srcElement || e.target;
  if (o.id == 'gotoedit') return;

  $('fp_editor').onclick = '';
  loadjs('js/wind_c_editor.js', '', '', function() {
    editor = new WYSIWYD();
    editor.config = new WysiwygConfig();
    editor.init();
    if (o.id && o.id.match(/^wy\_/)) {
      var key = o.id.substr(3);
      if (typeof editor.config.btnList[key]) {
        var cmd = editor.config.btnList[key][1];
        cmd(key);
      }
    }
  });
}

function showEmotion() {
	editor.saveRange();
	showDefault();
	setTimeout(function(){
	document.getElementById('pw_box').onclick=function(e)
	{
		if ( e && e.preventDefault )
			e.stopPropagation();
		else
			window.event.cancelBubble = true;
	};
	document.body.onclick=function(){
		closep();
		document.body.onclick=null;
		document.getElementById('pw_box').onclick=null;
	};},100);
	return false;
}

function inputfriends(){
	var friends = ajax.request.responseText.split(",").join(" @");
	var oldvalue = getObj('textarea').value;
	if (oldvalue && friends) {
		getObj('textarea').value =oldvalue + '@' + friends+' ';
	} else if (friends) {
		getObj('textarea').value = '@'+friends+' ';
	}
}